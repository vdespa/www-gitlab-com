---
layout: markdown_page
title: "Group Direction - Knowledge"
description: "Knowledge group direction page focusing on wiki, pages, markdown, and text editors"
canonical_path: "/direction/plan/knowledge/"
---

- TOC
{:toc}

## Knowledge group

| | |
| --- | --- |
| Stage | [Plan](/direction/plan/) |
| Content Last Reviewed | `2025-01-09` |

### Introduction

Welcome to the Knowledge group direction page! Knowledge group is in the Plan Stage of GitLab and contains the Wiki, Pages, Markdown, and Text Editors categories. Knowledge Group also manages the development of [GitLab Query Language](https://gitlab.com/groups/gitlab-org/gitlab-query-language/-/wikis/home) (GLQL), as well as Views, powered by GLQL. These categories, and GLQL, fall within the [knowledge management](https://www.gartner.com/reviews/market/knowledge-management-software) (KM) market as defined by Gartner. The focus of KM is to provide visibility and access to a flow of information across a variety of operations, enabling collaboration that has traditionally existed in silos. Commonly used products in the KM market include [Confluence](https://www.atlassian.com/software/confluence), and [Notion](https://www.notion.so/product). 

If you have feedback related to Knowledge Group please comment and/or contribute in these [issues](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=group%3A%3Aknowledge&first_page_size=20), or create a new issue if you find none fit your criteria. Make sure to tag `@mmacfarlane`, Product Manager for Knowledge group, so he can read and respond to your comment. Sharing your feedback via issues is the one of the best ways to contribute to our strategy and vision! 

### Our vision and current work

At GitLab, our [vision](https://about.gitlab.com/company/vision/) is to become the AllOps platform, a single application for all innovation. The Knowledge Group plays a pivotal role in this transformation by breaking down silos and enabling seamless collaboration across teams and roles. Here's how our Knowledge Group categories drive this vision forward:

Pages: Pages empowers everyone—from novice to expert—to host web content. We're enhancing this experience with GitLab Pages Parallel Deployments, streamlining the deployment process for all users. Looking ahead, we're exploring ways to make content management more intuitive for non-technical teams, including features like User Authentication and Authorization and comprehensive Audit Logs.

Text Editors and Markdown: We're expanding our Rich Text Editor across GitLab to unite technical and non-technical teams on a single platform—a key enabler of our AllOps vision. By delivering a consistent editing experience everywhere, we're breaking down traditional barriers between development and business teams. Our ongoing improvements in advanced editing features and performance ensure every team member can contribute effectively, fostering true cross-functional collaboration in one application.

Wiki: As a cornerstone of modern Agile Planning, our Wiki solution bridges documentation and development in the GitLab platform. We're actively expanding its capabilities with features like PDF export, WYSIWYG editing, and templates. Our latest enhancement, an experimental vision of Wiki comments, fosters direct collaboration and knowledge sharing within documentation. Building on our Jobs-to-be-Done research, we're evolving the Wiki to strengthen GitLab's integrated Agile Planning experience, enabling seamless knowledge sharing across the entire software development lifecycle.

### 1 year plan

Pages: Our 1 year plan for Pages can be broken into two distinct themes, stability and improvement. 

Our first theme, stability, focuses on the breakdown of existing bugs and maintenance items, as well as updating documentation related to previous releases. Specific to documentation, we've come to understand that some previous feature rollouts did not include updates to technical documentation, which has caused some confusion to users in utilizing the product. 

Our second theme, improvement, focuses on new feature development. We are close to wrapping up major feature development on Parallel Deployments and look forward to evaluate other opportunities in the space.

Text Editors and Markdown: Our Text Editors and Markdown strategy directly advances GitLab's AllOps vision by democratizing content creation across the platform. By implementing the Rich Text Editor throughout GitLab, we're breaking down traditional barriers between technical and non-technical teams. This unified editing experience accelerates collaboration and empowers every team member to contribute effectively, regardless of their technical expertise. Additionally, in the coming year we are aiming to begin development of collaborative editing.

Wiki: In FY24Q2 we completed research on the Wiki category in order to develop JTBD. We've finalized these [JTBD](https://gitlab.com/groups/gitlab-org/plan-stage/-/epics/54) and will utilize them to inform our Wiki strategy.

### What we recently completed

Text Editors and Markdown: [Rich Text Editor: Release Rich Text Editor Generally](https://about.gitlab.com/releases/2023/07/22/gitlab-16-2-released/#:~:text=The%20rich%20text%20editor%20is%20a%20new%20way%20of%20editing,can%20follow%20our%20progress%20here.), [Display and Edit Markdown Comments in the Rich Text Editor](https://gitlab.com/gitlab-org/gitlab/-/issues/342173), [Insert Table of Contents in the Rich Text Editor](https://gitlab.com/gitlab-org/gitlab/-/issues/366525).

Wiki: [Draw.io/diagrams.net integration with wiki](https://gitlab.com/gitlab-org/gitlab/-/issues/20305/?_gl=1*lfhu8t*_ga*MTU5MDI5ODc5NS4xNjY1NTkyMzcy*_ga_ENFH3X7M5Y*MTY4MTQwNDI2MC40MDYuMS4xNjgxNDA1MzI3LjAuMC4w), [Editing Sidebar in Project or Group Wiki Removes Existing Sidebar](https://gitlab.com/gitlab-org/gitlab/-/issues/378028),Relative Links are [Broken On Wiki ASCII Pages](https://gitlab.com/gitlab-org/gitlab/-/issues/377976), [templates in wiki](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/133279), and [autocomplete](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/133281).

Pages: [GitLab Pages without DNS Wildcard](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/131947) and [GitLab Pages Parallel Deployments Beta](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/132384).

### What we are currently working on

Pages: [GitLab Pages Parallel Deployments GA](https://gitlab.com/groups/gitlab-org/-/epics/10914)
 
Rich Text Editor: [Enabling the rich text editor across GitLab](https://gitlab.com/groups/gitlab-org/-/epics/10378)

Wiki: [GLQL implementation in GitLab, including Wiki](https://gitlab.com/groups/gitlab-org/-/epics/14767)

### Competitive analysis and best-in-class landscape

Knowledge Group Competitive Statement: The focus of Knowledge group is to provide visibility and access to a flow of information across a variety of operations, enabling collaboration that has traditionally existed in silos. Commonly used products in the market include Confluence, Notion, and GitHub. We believe that fundamental improvements in our Pages, Text Editors, Markdown, and Wiki categories enable us to deliver a more competitive planning experience against industry leaders.

Pages: We are invested in supporting the process of developing and deploying code from a single place as a convenience for our users. Other providers, such as Netlify, deliver a more comprehensive solution. There are project templates available that offer the use of Netlify for static site CI/CD, while also still taking advantage of GitLab for SCM, merge requests, issues, and everything else. GitLab offers [configurable redirects](https://gitlab.com/gitlab-org/gitlab-pages/-/issues/24), a well-loved feature of Netlify, made available in gitlab-pages. We are seeing a rise in JAMStack and static site generators partnering in the media. This trend toward API-first, affirms our modernization effort of Pages, reinforcing our cloud native installation maturity plan. GitHub also offers hosting of static sites with GitHub Pages. Key differentiators between the two are that GitHub Pages configuration and deployment is more "automatic" in that it doesn't require you to edit a CI configuration file, and that GitHub Pages has limits placed on bandwidth, builds, and artifact size where GitLab currently does not.

Text Editors and Markdown: These two categories do not necessarily have a direct competitor but they are important to our vision of becoming an AllOps platform.

Wiki: We currently most closely compete with GitHub Wiki but we would like to compete with Confluence, Notion, Roam Research, and Google Docs. We've heard from customers that managing wikis with tens of thousands of pages can be challenging. And while a full-featured product like Confluence has advanced features and integrations, the GitLab wiki would be a stronger competitor if we improved our sidebar structure and parity between Group and Project Wikis.