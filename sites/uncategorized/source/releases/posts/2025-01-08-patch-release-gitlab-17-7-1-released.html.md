---
title: "GitLab Patch Release: 17.7.1, 17.6.3, 17.5.5"
categories: releases
author: Greg Alfaro
author_gitlab: truegreg
author_twitter: gitlab
description: "Learn more about GitLab Patch Release: 17.7.1, 17.6.3, 17.5.5 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2025/01/08/patch-release-gitlab-17-7-1-released/'
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 17.7.1, 17.6.3, 17.5.5 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important bug and security fixes, and we strongly recommend that all self-managed GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version. GitLab Dedicated customers do not need to take action.

GitLab releases fixes for vulnerabilities in patch releases. There are two types of patch releases:
scheduled releases, and ad-hoc critical patches for high-severity vulnerabilities. Scheduled releases are released twice a month on the second and fourth Wednesdays.
For more information, you can visit our [releases handbook](https://handbook.gitlab.com/handbook/engineering/releases/) and [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of GitLab release blog posts [here](/releases/categories/releases/).

For security fixes, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are committed to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest patch release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

## Changes to Imports

GitLab released a new user contribution and membership mapping feature for GitLab importers, including Direct Transfer, GitHub, Bitbucket Server, and Gitea importers. This feature is available by default from GitLab 17.7.1. More information on the feature and availability can be found in a [blog post](https://about.gitlab.com/blog/2024/11/25/streamline-migrations-with-user-contribution-and-membership-mapping/) and in the documentation [here](https://docs.gitlab.com/ee/user/project/import/index.html#user-contribution-and-membership-mapping). 

### Why GitLab changed its importer functionality

Vulnerabilities ([CVE-2024-5655](https://nvd.nist.gov/vuln/detail/CVE-2024-5655), [CVE-2024-6385](https://nvd.nist.gov/vuln/detail/CVE-2024-6385), [CVE-2024-6678](https://nvd.nist.gov/vuln/detail/CVE-2024-6678), [CVE-2024-8970](https://nvd.nist.gov/vuln/detail/CVE-2024-8970)) affecting import functionality were discovered through our HackerOne bug bounty program. To address these vulnerabilities and further enhance security, GitLab redesigned the importers’ user contribution mapping functionality. 

### What’s changing? 

1. **Post-import mapping**: Previously unavailable, this feature allows you to assign imported contributions and memberships to users on the destination instance after completing the import. Imported memberships and contributions are first mapped to placeholder users. Until they are reassigned, contributions will be displayed as associated with placeholders.
1. **Email-independent mapping**: The new process doesn't rely on email addresses, allowing you to map contributions for users with different email addresses on source and destination instances.
1. **User control**: Each user on the destination instance assigned a contribution mapping must [accept the assignment](https://docs.gitlab.com/ee/user/project/import/#accept-contribution-reassignment) before any imported contributions are attributed to them. They can also [reject the assignment](https://docs.gitlab.com/ee/user/project/import/#reject-contribution-reassignment).

Full details describing improved user contribution and membership mapping features are available in the GitLab docs [here](https://docs.gitlab.com/ee/user/project/import/#user-contribution-and-membership-mapping). 

### Guidance for GitLab Self-Managed & Dedicated Customers

1. Exploitation requires that an attacker have an authenticated user account on the target GitLab instance. Therefore, the risk is primarily limited to insider threats unless you allow open internet access **and** public registrations.
1. GitLab strongly recommends disabling importers until your GitLab instance is upgraded to version 17.7.1 or later. You can disable import features by: 

    1. Logging in as a GitLab instance administrator user 
    2. Go to Admin > Settings > General > Import and Export settings
    3. Uncheck the box next to each enabled importer
    4. Click Save Changes

1. If you must enable an importer, GitLab recommends temporarily enabling it during an import and disabling the feature after the import is complete. 
1. GitLab Self-Managed with Direct Transfer (beta feature) or GitHub, Bitbucket Server, or Gitea importers enabled may be vulnerable and should be upgraded immediately. 


### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Security fixes

### Table of security fixes

| Title | Severity |
| ----- | -------- |
| [Possible access token exposure in GitLab logs](#possible-access-token-exposure-in-gitlab-logs) | Medium |
| [Cyclic reference of epics leads resource exhaustion](#cyclic-reference-of-epics-leads-resource-exhaustion) | Medium |
| [Unauthorized user can manipulate status of issues in public projects](#unauthorized-user-can-manipulate-status-of-issues-in-public-projects) | Medium |
| [Instance SAML does not respect `external_provider` configuration](#instance-saml-does-not-respect-external_provider-configuration) | Medium |

### Possible access token exposure in GitLab logs

An issue was discovered in GitLab CE/EE affecting all versions starting from 17.4 prior to 17.5.5, starting from 17.6 prior to 17.6.3, and starting from 17.7 prior to 17.7.1. Under certain conditions, access tokens may have been logged when API requests were made in a specific manner.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:H/I:H/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:H/I:H/A:N), 6.5).
It is now mitigated in the latest release and is assigned [CVE-2025-0194](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0194).

This vulnerability has been discovered internally by GitLab team member [Thong Kuah](https://gitlab.com/tkuah).


### Cyclic reference of epics leads resource exhaustion

An issue was discovered in GitLab CE/EE affecting all versions starting from 15.7 prior to 17.5.5, starting from 17.6 prior to 17.6.3, and starting from 17.7 prior to 17.7.1. It was possible to trigger a DoS by creating cyclic references between epics.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:L`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:L), 4.3).
It is now mitigated in the latest release and is assigned [CVE-2024-6324](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-6324).

Thanks [xorz](https://hackerone.com/xorz) for reporting this vulnerability through our HackerOne bug bounty program.


### Unauthorized user can manipulate status of issues in public projects

An issue was discovered in GitLab CE/EE affecting all versions starting from 15.5 before 17.5.5, 17.6 before 17.6.3, and 17.7 before 17.7.1, in which unauthorized users could manipulate the status of issues in public projects.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N), 4.3).
It is now mitigated in the latest release and is assigned [CVE-2024-12431](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-12431).

Thanks [pwnie](https://hackerone.com/pwnie) for reporting this vulnerability through our HackerOne bug bounty program.


### Instance SAML does not respect `external_provider` configuration

An issue was discovered in GitLab CE/EE affecting all versions starting from 16.4 prior to 17.5.5, starting from 17.6 prior to 17.6.3, and starting from 17.7 prior to 17.7.1. When a user is created via the SAML provider, the external groups setting overrides the external provider configuration. As a result, the user may not be marked as external thereby giving those users access to internal projects or groups.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:N), 4.3).
It is now mitigated in the latest release and is assigned [CVE-2024-13041](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-13041).

This vulnerability has been discovered internally by GitLab team member [Drew Blessing](https://gitlab.com/dblessing).

## Bug fixes


### 17.7.1

* [Cherry pick fix for gsutil into '17-7-stable'](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/2151)
* [backport 17.7.x: Downgrade grpc-go to v1.66.3](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/7533)
* [Backport  Revert "Merge branch 'improve_reference_rewriter_to_work_cross_groups' into 'master'"](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/176091)
* [17.7 Backport Reject system notes when indexing notes on work items](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/176128)
* [Merge branch 'andrey-fix-qa-spec' into 17.7](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/176577)
* [Merge branch 'release-unique-users' into 17.7](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/176356)
* [Fix CI job token signing key not always generated](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/176507)
* [Update acme-client to v2.0.19](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/176508)

### 17.6.3

* [Cherry pick '2125-base-force-upgrade-ubi' into '17-6-stable'](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/2125)
* [Cherry pick fix for gsutil into '17-6-stable'](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/2152)
* [backport 17.6.x: Downgrade grpc-go to v1.66.3](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/7535)
* [Backport Advanced Search: Set engine for OpenSearch indices](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/175367)
* [17.6: Fix ability to use password for Git when password for Web is disabled](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/175272)
* [Backport running release-environments QA from the stable branch to 17.6](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/175307)
* [Backport 'fix-env-var-for-release-environments-qa' 17.6](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/175596)
* [Backport 'dattang/fix-build-gdk-image-script' to 17.6](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/175859)
* [Backport fix for diff_files highlights preload to 17.6](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/175657)
* [Quarantines iteration qa spec](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/176514)
* [Quarantine outdated user_views_iteration_spec.rb](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/176738)
* [Quarantine date sensitive specs 17.6](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/176754)

### 17.5.5

* [Cherry pick '2125-base-force-upgrade-ubi' into '17-5-stable'](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/2126)
* [Cherry pick fix for gsutil into '17-5-stable'](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/2153)
* [backport 17.5.x: Update changelog for 17.5.0](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/7536)
* [Backport running release-environments QA from the stable branch to 17.5](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/175308)
* [Backport 'dattang/allow-release-environments-to-fail' to 17.5](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/175364)
* [Backport fix for diff_files highlights preload to 17.5](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/175733)
* [Backport 'dattang/fix-build-gdk-image-script' to 17.5](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/175858)
* [Quarantines iteration qa spec](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/176596)
* [Quarantine outdated user_views_iteration_spec.rb](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/176739)
* [Quarantine date sensitive specs 17.5](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/176753)
* [bump devfile gem to 0.0.28 patch](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/176568)
* [17.5: Fix ability to use password for Git when password for Web is disabled](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/175274)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Patch Notifications

To receive patch blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [patch release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).

## We’re combining patch and security releases

This improvement in our release process matches the industry standard and will help GitLab users get information about security and
bug fixes sooner, [read the blog post here](https://about.gitlab.com/blog/2024/03/26/were-combining-patch-and-security-releases/).
