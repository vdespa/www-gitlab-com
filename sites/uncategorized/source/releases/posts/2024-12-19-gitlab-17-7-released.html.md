---
release_number: "17.7" # version number - required
title: "GitLab 17.7 released with new Planner user role" # short title (no longer than 62 characters) - required
author: Courtney Meddaugh # author name and surname - required
author_gitlab: courtmeddaugh # author's gitlab.com username - required
image_title: '/images/17_7/17_7-cover-image.png' # cover image - required
description: "GitLab 17.7 released with a new Planner user role, auto-resolution policy for vulnerabilities, admin-controlled instance integration allowlists, access token rotation in the UI and much more!" # short description - required
twitter_image: '/images/17_7/17_7-cover-image.png' # required - copy URL from image title section above
categories: releases # required
layout: release # required
featured: yes
rebrand_cover_img: true

# APPEARANCE
# header_layout_dark: true #uncomment if the cover image is dark
# release_number_dark: true #uncomment if you want a dark release number
# release_number_image: "/images/X_Y/X_Y-release-number-image.svg" # uncomment if you want a svg image to replace the release number that normally overlays the background image

---

<!--
This is the release blog post file. Add here the introduction only.
All remaining content goes into data/release-posts/.

**Use the merge request template "Release-Post", and please set the calendar due
date for each stage (general contributions, review).**

Read through the Release Posts Handbook for more information:
https://about.gitlab.com/handbook/marketing/blog/release-posts/#introduction
-->

Today, we are excited to announce the release of GitLab 17.7 with a [new Planner user role](#new-planner-user-role), [auto-resolution policy for vulnerabilities](#auto-resolve-vulnerabilities-when-not-found-in-subsequent-scans), [admin-controlled instance integration allowlists](#instance-administrators-can-control-which-integrations-can-be-enabled), [access token rotation in the UI](#rotate-personal-project-and-group-access-tokens-in-the-ui) and much more!

These are just a few highlights from the 230+ improvements in this release. Read on to check out all of the great updates below.

To the wider GitLab community, thank you for the 138 contributions you provided to GitLab 17.7!
At GitLab, [everyone can contribute](https://about.gitlab.com/community/contribute/) and we couldn't have done it without you!

To preview what's coming in next month’s release, check out our [Upcoming Releases page](/direction/kickoff/), which includes our 17.8 release kickoff video.
