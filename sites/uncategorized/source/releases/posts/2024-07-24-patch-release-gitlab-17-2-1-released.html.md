---
title: "GitLab Patch Release: 17.2.1, 17.1.3, 17.0.5"
categories: releases
author: Greg Alfaro
author_gitlab: truegreg
author_twitter: gitlab
description: "Learn more about GitLab Patch Release: 17.2.1, 17.1.3, 17.0.5 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 17.2.1, 17.1.3, 17.0.5 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important bug and security fixes, and we strongly recommend that all GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version.

GitLab releases fixes for vulnerabilities in dedicated patch releases. There are two types of patch releases:
scheduled releases, and ad-hoc critical patches for high-severity vulnerabilities. Scheduled releases are released twice a month on the second and fourth Wednesdays.
For more information, you can visit our [releases handbook](https://handbook.gitlab.com/handbook/engineering/releases/) and [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of GitLab release blog posts [here](/releases/categories/releases/).

For security fixes, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are dedicated to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest patch release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Security fixes

### Table of security fixes

| Title | Severity |
| ----- | -------- |
| [XSS via the Maven Dependency Proxy](#xss-via-the-maven-dependency-proxy) | High |
| [Project level analytics settings leaked in DOM](#project-level-analytics-settings-leaked-in-dom) | Medium |
| [Reports can access and download job artifacts despite use of settings to prevent it](#reports-can-access-and-download-job-artifacts-despite-use-of-settings-to-prevent-it) | Medium |
| [Direct Transfer - Authorised project/group exports are accessible to other users](#direct-transfer---authorised-projectgroup-exports-are-accessible-to-other-users) | Medium |
| [Bypassing tag check and branch check through imports](#bypassing-tag-check-and-branch-check-through-imports) | Low |
| [Project Import/Export - Make project/group export files hidden to everyone except user who initiated it](#project-importexport---make-projectgroup-export-files-hidden-to-everyone-except-user-who-initiated-it) | Low |



### XSS via the Maven Dependency Proxy

A cross site scripting vulnerability exists in GitLab CE/EE affecting all versions from 16.6 prior to 17.0.5, 17.1 prior to 17.1.3, 17.2 prior to 17.2.1 allowing an attacker to execute arbitrary scripts under the context of the current logged in user.
This is a high severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:C/C:H/I:H/A:N`, 7.7)

This vulnerability has been discovered internally by GitLab team member [Joern Schneeweisz](https://gitlab.com/joernchen).

### Project level analytics settings leaked in DOM

An issue was discovered in GitLab EE affecting all versions starting from 16.11 prior to 17.0.5, 17.1 prior to 17.1.3, 17.2 prior to 17.2.1 where certain project-level analytics settings could be leaked in DOM to group members with Developer or higher roles.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:H/PR:H/UI:N/S:U/C:H/I:N/A:N `, 4.4).
It is now mitigated in the latest release and is assigned [CVE-2024-5067](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-5067).

Thanks [yvvdwf](https://hackerone.com/yvvdwf) and [zebraman](https://hackerone.com/zebraman) for reporting this vulnerability through our HackerOne bug bounty program.

### Reports can access and download job artifacts despite use of settings to prevent it

An information disclosure vulnerability in GitLab CE/EE affecting all versions starting from 16.7 prior to 17.0.5, starting from 17.1 prior to 17.1.3, and starting from 17.2 prior to 17.2.1 where job artifacts can be inappropriately exposed to users lacking the proper authorization level.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N`, 4.3).
It is now mitigated in the latest release and is assigned [CVE-2024-7057](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-7057).

Thanks [ricardobrito](https://hackerone.com/ricardobrito) for reporting this vulnerability through our HackerOne bug bounty program.

### Direct Transfer - Authorised project/group exports are accessible to other users

An issue was discovered in GitLab CE/EE affecting all versions starting from 15.6 prior to 17.0.5, starting from 17.1 prior to 17.1.3, and starting from 17.2 prior to 17.2.1 where it was possible to disclose limited information of an exported group or project to another user. 

This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:N/A:N`, 4.1 ).

This vulnerability was found internally by a GitLab team member [James Nutt](hhttps://gitlab.com/jnutt).

### Bypassing tag check and branch check through imports

A resource misdirection vulnerability in GitLab CE/EE affecting all versions 12.0 prior to 17.0.5, 17.1 prior to 17.1.3, and 17.2 prior to 17.2.1 allows an attacker to craft a repository import in such a way as to misdirect commits.
This is a low severity issue (`CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:N/I:L/A:N`, 2.7).
It is now mitigated in the latest release and is assigned [CVE-2024-0231](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-0231).

Thanks [aaron_dewes](https://hackerone.com/aaron_dewes) for reporting this vulnerability through our HackerOne bug bounty program.


### Project Import/Export - Make project/group export files hidden to everyone except user who initiated it

An information disclosure vulnerability in GitLab CE/EE in project/group exports affecting all versions from 15.4 prior to 17.0.5, 17.1 prior to 17.1.3, and 17.2 prior to 17.2.1 allows unauthorized users to view the resultant export.
This is a low severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:U/C:L/I:N/A:N`, 2.6).

This vulnerability has been discovered internally by GitLab team member [Martin Wortschack](https://gitlab.com/wortschi)


## Bug fixes


### 17.2.1

* [Revert "Ensure page token is for the same tree"](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/7103)
* [Fix order-dependent Elasticsearch spec failure](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159815)
* [Backport to run Release Environments on RC tag into '17-2-stable-ee'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159794)
* [Fix state leak in cluster_util_spec.rb](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159993)
* [Ensure rspec helpers call curl with --fail](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160006)
* [Run e2e:package-and-test-ee for MR targeting stable branches](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160047)
* [Remove build-gdk-image, e2e:test-on-gdk, and retag-gdk-image jobs (17.2)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160129)
* [17.2 backport for fix PEP when SEC is available](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160007)
* [bugfix: Only run advanced SAST job when Ultimate license present](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160276)
* [Backport pipeline fixes for 17.2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160377)
* [Private dotenv artifacts not accessible to downstream jobs](https://gitlab.com/gitlab-org/gitlab/-/issues/469443)

### 17.1.3

* [Backport mock tag cleanup related fixes](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1921)
* [Multiarch fixes backport (17.1)](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1904)
* [Backport release-environments pipeline in security repo to 17.1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158737)
* [Backport [17.1] Fix empty minimum_should_match in query](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158841)
* [Fix wildcard search for package.json in npm upload](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159016)
* [NPM registry: replace the saj parser (17.1 backport)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159075)
* [Fix Content-Disposition header for Azure in API download (17.1 Backport)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159701)
* [Fix order-dependent Elasticsearch spec failure](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159817)
* [Backport to run Release Environments on RC tag into '17-1-stable-ee'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159795)
* [Fix state leak in cluster_util_spec.rb](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159995)
* [Merge branch 'sh-curl-fail-ci' into 'master' - 17.1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160094)
* [Ignore object pool already exists creation errors 17.1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159003)
* [Backport token logging improvements](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158509)
* [Backport add Rake task to show token expiration info](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159049)
* [Remove build-gdk-image, e2e:test-on-gdk, and retag-gdk-image jobs (17.1)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160128)
* [Backport pipeline fixes for 17.1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160392)
* [Private dotenv artifacts not accessible to downstream jobs](https://gitlab.com/gitlab-org/gitlab/-/issues/469443)

### 17.0.5

* [Backport mock tag cleanup related fixes](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1920)
* [Multiarch fixes backport (17.0)](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1903)
* [Backport to run Release Environments on RC tag into '17-0-stable-ee'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159796)
* [Backport Resolve "Geo: JWT token expiration too short"](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159286)
* [Ignore object pool already exists creation errors 17.0](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159019)
* [Fix 500 error using a instance runner registration token](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158907)
* [Backport token logging improvements](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158511)
* [Drop migration that finalizes migration to add PAT expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158266)
* [Update the token expiration banner](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159417)
* [Backport add Rake task to show token expiration info](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159054)
* [Fix order-dependent custom role definition spec failure](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160326)
* [Backport pipeline fixes for 17.0](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160393)
* [Private dotenv artifacts not accessible to downstream jobs](https://gitlab.com/gitlab-org/gitlab/-/issues/469443)

### 16.11.7
* [Backport Resolve "Geo: JWT token expiration too short"](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159287)
* [Ignore object pool already exists creation errors 17.0](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159019)
* [Backport token logging improvements](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/157277)
* [Drop migration that finalizes migration to add PAT expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158416)
* [Update the token expiration banner](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159418)
* [Backport add Rake task to show token expiration info](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159056)

### 16.10.9
* [Backport token logging improvements](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158516)
* [Drop migration that finalizes migration to add PAT expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158414)
* [Update the token expiration banner](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159419)
* [Backport add Rake task to show token expiration info](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159057)

### 16.9.10
* [Backport token logging improvements](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158517)
* [Drop migration that finalizes migration to add PAT expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/153470)
* [Update the token expiration banner](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159421)
* [Backport add Rake task to show token expiration info](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159058)

### 16.8.9
* [Backport token logging improvements](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158518)
* [Drop migration that finalizes migration to add PAT expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/153469)
* [Update the token expiration banner](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159424)
* [Backport add Rake task to show token expiration info](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159066)

### 16.7.9
* [Backport token logging improvements](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158519)
* [Drop migration that finalizes migration to add PAT expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/153468)
* [Update the token expiration banner](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159426)
* [Backport add Rake task to show token expiration info](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159069)

### 16.6.9
* [Backport token logging improvements](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158525)
* [Drop migration that finalizes migration to add PAT expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/153466)
* [Update the token expiration banner](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159429)
* [Backport add Rake task to show token expiration info](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159089)

### 16.5.9
* [Backport token logging improvements](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158526)
* [Drop migration that finalizes migration to add PAT expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/153459)
* [Add the token expiration banner](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158110)
* [Backport add Rake task to show token expiration info](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159093)

### 16.4.6
* [Drop migration that finalizes migration to add PAT expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/153458)
* [Add the token expiration banner](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158474)
* [Backport add Rake task to show token expiration info](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159125)

### 16.3.8
* [Drop migration that finalizes migration to add PAT expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/153457)
* [Add the token expiration banner](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158475)
* [Backport add Rake task to show token expiration info](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159383)

### 16.2.10
* [Drop migration that finalizes migration to add PAT expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/153453)
* [Add the token expiration banner](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158479)
* [Backport add Rake task to show token expiration info](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159401)

### 16.1.7
* [Drop migration that finalizes migration to add PAT expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/153448)
* [Add the token expiration banner](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158476)
* [Backport add Rake task to show token expiration info](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159414)

### 16.0.9
* [Drop migration that finalizes migration to add PAT expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/153435)
* [Add the token expiration banner](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158477)
* [Backport add Rake task to show token expiration info](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159446)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Patch Notifications

To receive patch blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [patch release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).

## We’re combining patch and security releases

This improvement in our release process matches the industry standard and will help GitLab users get information about security and
bug fixes sooner, [read the blog post here](https://about.gitlab.com/blog/2024/03/26/were-combining-patch-and-security-releases/).
