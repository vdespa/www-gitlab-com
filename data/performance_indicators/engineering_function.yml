- name: Engineering Hiring Actual vs Plan
  base_path: "/handbook/engineering/performance-indicators/"
  definition: Employees are in the division `Engineering`.
  target: Confidential, in Adaptive
  org: Engineering Function
  health:
    level: 0
    reasons:
    - Data currently lives in Adaptive, not available in Sisense.
    - Waiting on the data becoming available
  urls:
- name: Engineering Budget Plan vs Actuals
  base_path: "/handbook/engineering/performance-indicators/"
  definition: We need to spend our investors' money wisely. We also need to run a
    responsible business to be successful.
  target: Confidential, in Adaptive
  org: Engineering Function
  is_key: false
  health:
    level: 0
    reasons:
    - Data currently lives in Adaptive, not available in Sisense.
    - Waiting on the data becoming available
  urls:
- name: Diversity
  base_path: "/handbook/engineering/performance-indicators/"
  definition: Diversity, Inclusion & Belonging  is one of our core values, and a general
    challenge for the tech industry. GitLab is in a privileged position to positively
    impact diversity in tech because our remote lifestyle should be more friendly
    to people who may have left the tech industry, or studied a technical field but
    never entered industry. This means we can add to the diversity of our industry,
    and not just play a zero-sum recruiting game with our competitors.
  target: It's against company policy to set diversity quotas (but we may add benchmarks
    to compare ourselves against).
  org: Engineering Function
  is_key: false
  health:
    level: 2
    reasons:
    - Engineering is now at the tech benchmark for gender diversity (~16%), but our
      potential is greater and we can do better. 20% should be our floor in technical
      roles. Other types of diversity are unknown.
    - Get data wired up and visualized in Tableau.
    - Clear data sharing with legal.
    - Get better data about racial and country diversity.
  urls:
  - https://docs.google.com/presentation/d/1Rj6brdP2hKU1zgj0ll1poywBw0aR9jIAdT4GsLZfIIU/edit#slide=id.g4bb9f44e0b_6_160
  - https://docs.google.com/spreadsheets/d/1Jef9oopd8AeZRYM2P9B8ye2TxfdzxI0FY6ixhlyzW_c/edit#gid=1035217495
- name: Engineering Handbook MR Rate
  base_path: "/handbook/engineering/performance-indicators/"
  definition: |
    The handbook is essential to working remote successfully, to keeping
    up our transparency, and to recruiting successfully. Our processes are constantly
    evolving and we need a way to make sure the handbook is being updated at a regular
    cadence. This is measured by Merge Requests that update the handbook contents relate to the Engineering Division overtime.
  target: Greater than 0.55 per person per month
  org: Engineering Function
  is_key: true
  health:
    level: 3
    reasons:
    - Above target
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/HandbookMRRate/HandbookMRRate
        height: 300px
        toolbar: hidden
        hide_tabs: true
        parameters:
          - name: Department/Division
            value: Engineering
- name: Merge Requests Types
  base_path: "/handbook/engineering/performance-indicators/"
  definition: Distribution of monthly Merge Requests that are categorized into types of feature work. Includes work on; new features, enhancements, maintenance and bug fixes <a href="/handbook/engineering/metrics/#projects-that-are-part-of-the-product">projects that are part of the product</a>.
  org: Engineering Function
  is_key: false
  health:
    level: 0
    reasons:
      - Metric has no target and is being monitored
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/MergeRequestMetrics/OverallMRsbyType_1
        height: 300px
        toolbar: hidden
        hide_tabs: true
- name: Engineering Discretionary Bonus Rate
  base_path: "/handbook/engineering/performance-indicators/"
  definition: The number of discretionary bonuses given divided by the total number of team members, in a given period as defined. This metric definition is taken from the <a href="/handbook/people-group/people-success-performance-indicators/#discretionary-bonuses">People Success Discretionary Bonuses KPI</a>.
  target: at or above 10%
  org: Engineering Function
  is_key: false
  health:
    level: 2
    reasons:
      - Metric is new and is being monitored
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/PeopleAnalyticsHandbookEmbedding/R3DiscretionaryBonusRate
        height: 300px
        toolbar: hidden
        hide_tabs: true
        filters:
          - field: DPT Modified Division
            value: Engineering
- name: Engineering Team Member Retention
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: We need to be able to retain talented team members. Retention measures our ability to keep them sticking around at GitLab. Team Member Retention = (1-(Number of Team Members leaving GitLab/Average of the 12 month Total Team Member Headcount)) x 100. GitLab measures team member retention over a rolling 12 month period. This metric definition is taken from the <a href="/handbook/people-group/people-success-performance-indicators/#team-member-retention-rolling-12-months">People Success Team Member Retention KPI</a>.
  target: at or above 84%
  org: Engineering Function
  is_key: true
  public: false
  health:
    level: 3
    reasons:
    - above target, constant trend
  urls:
    - "https://10az.online.tableau.com/t/gitlab/views/PeopleAnalyticsHandbookLinksOnly/R12RetentionRate/74331d93-398e-4f46-a963-2f54e705bda6/25fe5987-23f2-47bb-bc73-97d847e23865" 
- name: Engineering Vacancy Time to Fill
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Vacancy Time to Fill measures the average time job openings take from open to close. This metric includes sourcing time of candidates compared to Time to Hire or Time to Offer Accept which only measures the time from when a candidate applies to when they accept.
  target: at or below 50 days
  org: Engineering Function
  is_key: true
  public: true
  health:
    level: 2
    reasons:
    - Trending up
    - Need to coach hiring managers to lean in while recruiting rebuilds
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/PeopleAnalyticsHandbookEmbedding/ReqAging
        filters:
          - field: Req Cost Center Name
            value: Engineering
- name: Engineering Promotion Rate
  base_path: "/handbook/engineering/performance-indicators/"
  definition: The total number of promotions over a rolling 12 month period divided by the month end headcount. The target promotion rate is 12% of the population. This metric definition is taken from the  <a href="/handbook/people-group/people-success-performance-indicators/#promotion-rate">People Success Team Member Promotion Rate PI</a>.
  target: 12%
  org: Engineering Function
  is_key: false
  health:
    level: 3
    reasons:
      - Metric is new and is being monitored
      - On 2021-06-23 People Success received updated industry data. They will be evaluating our data against these benchmarks and adjust our targets as applicable.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/PeopleAnalyticsHandbookEmbedding/Promo-Q
        height: 300px
        toolbar: hidden
        hide_tabs: true
        filters:
          - field: DPT Modified Division
            value: Engineering
