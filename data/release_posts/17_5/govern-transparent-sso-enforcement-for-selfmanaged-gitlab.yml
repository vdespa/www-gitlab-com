---
features:
  secondary:
  - name: "Selective SAML single sign-on enforcement"
    available_in: [core, premium, ultimate]
    gitlab_com: false
    add_ons: []
    documentation_link: 'https://docs.gitlab.com/ee/administration/settings/sign_in_restrictions.html#disable-password-authentication-for-users-with-an-sso-identity'
    reporter: hsutor
    stage: software_supply_chain_security
    categories:
    - User Management
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/382917'
    description: |
      Previously, when SAML SSO was enabled, groups could choose to enforce SSO, which required all members to use SSO
      authentication to access the group. However, some groups want the security of SSO enforcement for employees or
      group members, while still allowing outside collaborators or contractors to access their groups without SSO.

      Now, groups with SAML SSO enabled have SSO automatically enforced for all members
      who have a SAML identity. Group members without SAML identities are not required to
      use SSO unless SSO enforcement is explicitly enabled.

      A member has a SAML identity if one or both of the following are true:

      - They signed in to GitLab using their GitLab group's single sign-on URL.
      - They were provisioned by SCIM.

      To ensure smooth operation of the selective SSO enforcement feature, ensure your SAML configuration is
      working properly before selecting the **Enable SAML authentication for this group** checkbox.
    force_right: true
