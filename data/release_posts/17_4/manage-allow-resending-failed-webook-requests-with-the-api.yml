---
features:
  secondary:
  - name: "Resend failed webhook requests with the API"
    available_in: [core, premium, ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/api/projects.html#resend-project-hook-event'
    reporter: m_frankiewicz
    stage: foundations
    categories:
    - Webhooks
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/372826'
    description: |
      Previously, GitLab provided the ability to resend webhook requests only in the UI, which was inefficient if many
      requests failed.

      So that you can handle failed webhook requests programmatically, in this release thanks to a community contribution, we
      added API endpoints for resending them:

      - [Project webhook requests](https://docs.gitlab.com/ee/api/projects.html#resend-project-hook-event)
      - [Group webhook requests](https://docs.gitlab.com/ee/api/groups.html#resend-group-hook-event) (Premium and Ultimate tier only)

      You can now:

      1. Get a list of [project hook](https://docs.gitlab.com/ee/api/projects.html#get-project-hook-events) or [group hook](https://docs.gitlab.com/ee/api/groups.html#get-group-hook-events) events.
      1. Filter the list to see failures.
      1. Use the `id` of any event to resend it.

      Thanks to [Phawin](https://gitlab.com/lifez) for [this community contribution](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/151130)!
