---
features:
  primary:
  - name: "Run multiple Pages sites with parallel deployments"
    available_in: [premium, ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/project/pages/#parallel-deployments'
    image_url: '/images/unreleased/pages_pd_ga.png'
    reporter: mmacfarlane
    stage: plan
    categories:
    - Pages
    epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/14434'
    description: |
      You can now create multiple versions of your GitLab Pages sites simultaneously with parallel deployments. Each deployment gets a unique URL based on your configured prefix. For example, with a unique domain your site would be accessible at `project-123456.gitlab.io/prefix`, or without a unique domain at `namespace.gitlab.io/project/prefix`.

      This feature is especially helpful when you need to:

      - Preview design changes or content updates.
      - Test site changes in development.
      - Review changes from merge requests.
      - Maintain multiple site versions (for example, with localized content).

      Parallel deployments expire after 24 hours by default to help manage storage space, though you can customize this duration or set deployments to never expire. For automatic cleanup, parallel deployments created from merge requests are deleted when the merge request is merged or closed.
